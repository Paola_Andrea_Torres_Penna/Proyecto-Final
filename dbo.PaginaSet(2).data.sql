﻿SET IDENTITY_INSERT [dbo].[PaginaSet] ON
INSERT INTO [dbo].[PaginaSet] ([Id_Pagina], [Quienes], [Servicios], [Visitanos], [Inicio]) VALUES (12, N'<head>
		<title>Quienes</title>
	</head>
	<body>
            <div class="about-pg">
                <h3>Quienes Somos</h3>
                <div class="about-pg">
                </div>
            </div>
                    <div class="container">
                        <div class="col-md-6 lt">
                            <div class="choose-us">
                                <h2 align ="center">¿POR QUÉ ELEGIRNOS?</h2>
                                <br />
                                <p align="justify">
                                    Centro Clínico +kotas PetShop es una entidad que presta servicios médicos veterinarios y complementarios para la atencion de pequeñas especies; con el objetivo de satisfacer la expectativas de los clientes y brindar el mejor servicio a través de un personal idóneo y capacitado para atender cualquier eventualidad que se presente al paciente.
                                    <br>
                                    <br>
                                    Actualmente somos una empresa sólida que día a día trabaja por el bienestar de las familias y de sus mascotas. Nuestro compromiso es mantener el liderazgo en el sector ofreciendo una mejor calidad de vida a las mascotas de las familias a través de las diversas especialidades médicas veterinarias que ofrece nuestra entidad.
                                    <br />
                                </p>
                                <br />
                                <div align="center">
                                    <img src="../images/18.jpg" alt="" title="" class="img-responsive" />
                                </div>
                                <br />
                            </div>
                        </div>
                        <div class="col-md-6 lt">
                            <div class="few-words">
                                 <h2 align="center">NUESTRO EQUIPO</h2>
                                <div align="center">
                                    <img src="../images/ss.jpg" alt="" title="" class="img-responsive" />
                                <br />
                                </div>

                                <div class="clear all"></div>
                                <p align="justify">Contamos con un equipo de trabajo idóneo, con principios éticos y morales solidos; 
                                comprometidos en poner a su disposición el conocimiento y la experiencia que contribuya al crecimiento y mejoramiento del buen desarrollo físico y mental de su mascota.
                                   
                                <br />
                                <br />Nuestro equipo médico, está en condiciones de prestar los servicios de:

                                    Medicina Preventiva, Cirugía  General, Emergencias Veterinarias, Tratamientos Médicos y
                                    Especialidades Veterinarias.</p>

                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                
        
	</body>', N'	<head>
		<title>Servicios</title>
	</head>
	<body>
		
			<div class="about-pg">
			<h3><strong>Nuestros Servicios</strong></h3>
                <div class="about-pg">
                </div>
            </div>
                    <div class="container">
                        <div class="choose-us">
                            <ul>
                                <li class="text-ul">
                                    <div class="folt">
                                        <h2>Urgencias</h2>
                                        <br />
                                        <p align="justify">
                                            <img src="../images/25.jpg" align="right">
                                            </ br>Para El Centro Clinico +kotas PetShop siempre es importante que puedas hacer conexión de asesoría en caso de cualquier urgencia que se presente, es por ello que brindamos nuestros servicios las 24 horas del día 
                                            y contamos con equipos especializados en el manejo de pacientes en estado crítico que nos permitan la estabilización y la intervención quirúrgica en caso de requerirlo el paciente.
                                            Contamos para esto con personal médico interdisciplinario calificado para una mejor atención de consulta externa y de urgencias de pequeños animales de día y de noche, días normales y festivos, 
                                            apoyados por una infraestructura y equipos especializados para dicha labor.
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li class="text-ul">
                                    <div class="folt">
                                        <h2>Consulta Especializada</h2>
                                        <p align="justify">
                                            <img src="../images/41.jpg" align="right">
                                            <br>Nuestra clínica se apoya en el diagnóstico de especialistas en asistencia técnica,cirugias, dermatología, medicamentos, ortopedia y medicina interna de ayuda diagnostica que nos permite estar más comprometidos con el diagnóstico de tu mascota.
                                            En nuestra consulta veterinaria especializada tu mascota será examinada de manera exhaustiva revisando su estado físico; nos encargamos  de  tomar todas sus constantes para determinar cualquier tipo de patología presente y así sugerir un tratamiento en base a sus hallazgos clínicos y pruebas de diagnostico complementarias. 
                                            El seguimiento a nuestros pacientes se realizarán a través de tres controles que serán programados sin costo adicional hasta dar por terminado el proceso; tenemos un sistema adecuado para la recolección de historias clínicas llevando paso a paso las citas de tu mascota.
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li class="text-ul">
                                    <div class="folt">

                                        <h2>Peluqueria</h2>
                                        <p align="justify">

                                            <img src="../images/27.jpg" align="right">
                                            <br>Al igual que las personas, nuestros pequeños integrantes de la familia necesitan de atención regular para verse y sentirse bien. Tanto los perros como los gatos necesitan un arreglo, estético, regular, que incluye todas las partes de su cuerpo.
                                            Nuestro personal de peluquería canina será el encargado de dejar hermosa y aseada a tu mascota.

                                            <br />El servicio de peluquería para perros incluye los siguientes items:

                                            <br>- Corte de pelo según la raza.
                                            <br>- Baño.
                                            <br>- Corte de uñas.
                                            <br>- Limpieza de oídos.
                                            <br>- Enjuague bucal.
                                            <br>- Pañoleta o moños.
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li class="text-ul">
                                    <div class="folt">
                                        <h2>Guarderia</h2>
                                        <p align="justify">
                                            <img src="../images/29.jpg" align="right">
                                            <br />Contamos con un sitio cómodo para tu mascota con atención permanente para que este seguro y feliz, enun ambiente cálido; escuchamos y asesoramos a nuestros clientes para identificar y definir según el comportamiento de la mascota 
                                            el lugar donde será alojado para garantizar que el tiempo de hospedaje en nuestra institución sea semejante a la comodidad donde habitualmente reside la mascota; puedes acomodar los horarios como tu prefieras para la estancia en nuestra 
                                            guardería canina por: Días, semanas y meses.
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
	</body>', N'<head>
    <title>Contáctenos</title>
</head>
<body>
    <!--contact-->
    <div class="contact-page">
        <h3>VISITENOS</h3>
        <div class="about-pg">
        </div>
        <div class="container">
            <p>
                   <h4>
                       <br />Dirección: Calle 4# 5-80, Barrio centro, San Vicente del Caguan. 
                            <br />
                       <br />Correo Electrónico: maskotaspetshop@gmail.com
                         <br />
                        <br />Teléfono: 3123005869- 3125882627
                   </h4>
            </p>
            <br />
            <div class="col-xs-4 col-md-4 tt brk2">
                <img src="../images/in.jpg" alt="cat" align="right" class="img-responsive">
            </div>
            <div class="col-xs-4 col-md-4 tt brk2">
                <img src="../images/ip.jpg" alt="cat" align="right" class="img-responsive">
            </div>
            <div class="col-xs-4 col-md-4 tt brk2">
                <img src="../images/im.jpg" alt="cat" align="right" class="img-responsive">
            </div>

        </div>
    </div>
    </body>', N'<div class="our-products">
    <div class="container">
        <div class="Quienes-somos">
            <div class="col-md-6 tt">
                <ul class="traits-text">
                    <li>
                        <p align="justify">
                            El Centro Clinico +kotas PetShop es una entidad que presta servicios médicos veterinarios y complementarios 
                            para la atencion de pequeñas especies; con el objetivo de satisfacer la expectativas de los clientes y brindar el mejor servicio a través de un personal idóneo y capacitado para atender cualquier eventualidad que se presente al paciente.
                            <br>
                        <p align=center><strong>Tu amigo más fiel merece lo mejor</strong></p>
                    </li>
                </ul>
                <span><a href="quienes.cshtml"  class="btn btn-danger bt" role="button" >Mas...</a></span>
            </div>
            <div class="col-md-5 tt">
                <div class="traits-logos">
                    <div class="col-xs-6 col-md-6 tt brk2">
                        <img src="../images/09.jpg" alt="cat" align="right" class="img-responsive">
                    </div>
                    <div class="col-xs-6 col-md-6 tt brk2">
                        <img src="../images/07.jpg" alt="cat" align="right" class="img-responsive">
                    </div>
                    <div class="col-xs-6 col-md-6 tt brk2">
                        <img src="../images/06.jpg" alt="cat" align="right" class="img-responsive">
                    </div>
                    <div class="col-xs-6 col-md-6 tt brk2">
                        <img src="../images/08.jpg" alt="cat" align="right" class="img-responsive">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="products-gallery">
            <h2>Nuestros Servicios</h2>
            <div class="col-md-3 arr">
                <div class="bg">
                    <img src="../images/25.jpg" alt="pet" class="img-responsive">
                    <span class="glyphicon glyphicon-heart pst" aria-hidden="true"></span>
                    <div class="caption">
                        <h3>Urgencias</h3>
                        <p>Para El Centro Clinico +kotas PetShop siempre es importante que...</p>
                        <p><a href="servicios.cshtml" class="btn btn-danger" role="button">Mas...</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 arr">
                <div class="bg">
                    <img src="../images/41.jpg" alt="pet" class="img-responsive">
                    <span class="glyphicon glyphicon-heart pst" aria-hidden="true"></span>
                    <div class="caption">
                        <h3>Consultas</h3>
                        <p>Nuestra clinica se apoya en el diagnóstico de especialistas en...</p>
                        <p><a href="servicios.cshtml" class="btn btn-danger" role="button">Mas...</a> </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 arr">
                <div class="bg">
                    <img src="../images/27.jpg" alt="pet" class="img-responsive">
                    <span class="glyphicon glyphicon-heart pst" aria-hidden="true"></span>
                    <div class="caption">
                        <h3>Peluqueria</h3>
                        <p>Al igual que las personas, nuestros pequeños integrantes de la familia...</p>
                        <p><a href="servicios.cshtml" class="btn btn-danger" role="button">Mas...</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 arr">
                <div class="bg">
                    <img src="../images/29.jpg" alt="pet" class="img-responsive">
                    <span class="glyphicon glyphicon-heart pst" aria-hidden="true"></span>
                    <div class="caption">
                        <h3>Guarderia</h3>
                        <p>Contamos con un sitio cómodo para su mascota con atencion permanente...</p>
                        <p><a href="servicios.cshtml" class="btn btn-danger" role="button">Mas...</a></p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>
</div>
<!--content-ends-->
<!--brand-logos-->
<div class="brand-logo">
    <div class="container">
        <div class="col-xs-6 col-md-3 brk3">
            <a href=""><img src="../images/p.png" alt="" class="img-responsive" /></a>
        </div>
        <div class="col-xs-6 col-md-3 brk3">
            <a href=""><img src="../images/g.png" alt="" class="img-responsive" /></a>
        </div>
        <div class="col-xs-6 col-md-3 brk3">
            <a href=""><img src="../images/t.png" alt="" class="img-responsive" /></a>
        </div>
        <div class="col-xs-6 col-md-3 brk3">
            <a href=""><img src="../images/s.png" alt="" class="img-responsive" /></a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>')
SET IDENTITY_INSERT [dbo].[PaginaSet] OFF
