﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Veterinaria.Models;

namespace Veterinaria.Controllers
{
    public class HomeController : Controller
    {
        private VeterinariaContainer db = new VeterinariaContainer();

        public ActionResult Index()
        {
            ViewBag.Message = db.PaginaSet.ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = db.PaginaSet.ToList();
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = db.PaginaSet.ToList();
            return View();
        }

        public ActionResult Servicios()
        {
            ViewBag.Message = db.PaginaSet.ToList();
            return View();
        }

        public ActionResult Inicio()
        {
            ViewBag.Message = db.PaginaSet.ToList();
            return View();
        }
    }
}