﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Veterinaria.Models;

namespace Veterinaria.Controllers
{
    public class Historia_ClinicaController : Controller
    {
        private VeterinariaContainer db = new VeterinariaContainer();

        // GET: Historia_Clinica
        public ActionResult Index(int id)
        {
            ViewBag.Mascota = db.MascotaSet.SingleOrDefault(x => x.Id_Mascota == id);
            return View(db.Historia_ClinicaSet.Where(x => x.Id_Mascota == id).ToList());
        }

        // GET: Historia_Clinica/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Historia_Clinica historia_Clinica = db.Historia_ClinicaSet.Find(id);
            if (historia_Clinica == null)
            {
                return HttpNotFound();
            }
            return View(historia_Clinica);
        }

        // GET: Historia_Clinica/Create
        public ActionResult Create(int id)
        {
            ViewBag.Mascota = db.MascotaSet.SingleOrDefault(x => x.Id_Mascota == id);
            return View(new Historia_Clinica
            {
                Id_Mascota = id
            });
        }

        // POST: Historia_Clinica/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Historia_Clinica historia_Clinica)
        {
            if (ModelState.IsValid)
            {
                db.Historia_ClinicaSet.Add(historia_Clinica);
                db.SaveChanges();
                return RedirectToAction("Index/" + historia_Clinica.Id_Mascota);
            }

            return View(historia_Clinica);
        }

        // GET: Historia_Clinica/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Historia_Clinica historia_Clinica = db.Historia_ClinicaSet.Find(id);
            if (historia_Clinica == null)
            {
                return HttpNotFound();
            }
            return View(historia_Clinica);
        }

        // POST: Historia_Clinica/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Historia_Clinica,Diagnostico,Sintomas,Id_Mascota")] Historia_Clinica historia_Clinica)
        {
            if (ModelState.IsValid)
            {
                db.Entry(historia_Clinica).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(historia_Clinica);
        }

        // GET: Historia_Clinica/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Historia_Clinica historia_Clinica = db.Historia_ClinicaSet.Find(id);
            if (historia_Clinica == null)
            {
                return HttpNotFound();
            }
            return View(historia_Clinica);
        }

        // POST: Historia_Clinica/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Historia_Clinica historia_Clinica = db.Historia_ClinicaSet.Find(id);
            db.Historia_ClinicaSet.Remove(historia_Clinica);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
