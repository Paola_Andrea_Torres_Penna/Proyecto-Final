﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Veterinaria.Models;

namespace Veterinaria.Controllers
{
    public class PaginasController : Controller
    {
        private VeterinariaContainer db = new VeterinariaContainer();

        // GET: Paginas
        public ActionResult Index()
        {
            return View(db.PaginaSet.ToList());
        }

        // GET: Paginas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pagina pagina = db.PaginaSet.Find(id);
            if (pagina == null)
            {
                return HttpNotFound();
            }
            return View(pagina);
        }

        // GET: Paginas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Paginas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Pagina,Quienes,Servicios,Visitanos,Inicio")] Pagina pagina)
        {
            if (ModelState.IsValid)
            {
                db.PaginaSet.Add(pagina);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pagina);
        }

        // GET: Paginas/Edit/5
        public ActionResult Edit(string id)
        {
            ViewBag.Content = id;
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pagina pagina = db.PaginaSet.FirstOrDefault();
            if (pagina == null)
            {
                return HttpNotFound();
            }
            return View(pagina);
        }

        // POST: Paginas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Pagina pagina)
        {
            if (ModelState.IsValid)
            {
                var page = db.PaginaSet.FirstOrDefault();
                page.Quienes = !string.IsNullOrEmpty(pagina.Quienes) ? pagina.Quienes : page.Quienes;
                page.Servicios = !string.IsNullOrEmpty(pagina.Servicios) ? pagina.Servicios : page.Servicios;
                page.Visitanos = !string.IsNullOrEmpty(pagina.Visitanos) ? pagina.Visitanos : page.Visitanos;
                page.Inicio = !string.IsNullOrEmpty(pagina.Inicio) ? pagina.Inicio : page.Inicio;

                //db.Entry(pagina).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pagina);
        }

        // GET: Paginas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pagina pagina = db.PaginaSet.Find(id);
            if (pagina == null)
            {
                return HttpNotFound();
            }
            return View(pagina);
        }

        // POST: Paginas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pagina pagina = db.PaginaSet.Find(id);
            db.PaginaSet.Remove(pagina);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
