//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Veterinaria.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Mascota
    {
        public int Id_Mascota { get; set; }
        public string Nombre { get; set; }
        public string Edad { get; set; }
        public string Color { get; set; }
        public string Raza { get; set; }
        public string Especie { get; set; }
        public string Peso { get; set; }
        public int Id_Propietario { get; set; }
    }
}
