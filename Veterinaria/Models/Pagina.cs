//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Veterinaria.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Pagina
    {
        public int Id_Pagina { get; set; }
        public string Quienes { get; set; }
        public string Servicios { get; set; }
        public string Visitanos { get; set; }
        public string Inicio { get; set; }
    }
}
