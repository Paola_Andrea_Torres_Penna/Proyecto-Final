
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/31/2015 22:16:50
-- Generated from EDMX file: D:\Documents\Visual Studio 2013\Projects\Veterinaria\Veterinaria\Models\Veterinaria.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Clinica];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[MascotaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MascotaSet];
GO
IF OBJECT_ID(N'[dbo].[PropietarioSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropietarioSet];
GO
IF OBJECT_ID(N'[dbo].[AdministradorSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AdministradorSet];
GO
IF OBJECT_ID(N'[dbo].[PaginaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PaginaSet];
GO
IF OBJECT_ID(N'[dbo].[Historia_ClinicaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Historia_ClinicaSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'MascotaSet'
CREATE TABLE [dbo].[MascotaSet] (
    [Id_Mascota] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Edad] nvarchar(max)  NOT NULL,
    [Color] nvarchar(max)  NOT NULL,
    [Raza] nvarchar(max)  NOT NULL,
    [Especie] nvarchar(max)  NOT NULL,
    [Peso] nvarchar(max)  NOT NULL,
    [Id_Propietario] int  NOT NULL
);
GO

-- Creating table 'PropietarioSet'
CREATE TABLE [dbo].[PropietarioSet] (
    [Id_Propietario] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Edad] nvarchar(max)  NOT NULL,
    [Direccion] nvarchar(max)  NOT NULL,
    [Correo] nvarchar(max)  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'AdministradorSet'
CREATE TABLE [dbo].[AdministradorSet] (
    [Id_Administrador] int IDENTITY(1,1) NOT NULL,
    [Nick] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PaginaSet'
CREATE TABLE [dbo].[PaginaSet] (
    [Id_Pagina] int IDENTITY(1,1) NOT NULL,
    [Quienes] nvarchar(max)  NOT NULL,
    [Servicios] nvarchar(max)  NOT NULL,
    [Visitanos] nvarchar(max)  NOT NULL,
    [Inicio] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Historia_ClinicaSet'
CREATE TABLE [dbo].[Historia_ClinicaSet] (
    [Id_Historia_Clinica] int IDENTITY(1,1) NOT NULL,
    [Diagnostico] nvarchar(max)  NOT NULL,
    [Sintomas] nvarchar(max)  NOT NULL,
    [Id_Mascota] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id_Mascota] in table 'MascotaSet'
ALTER TABLE [dbo].[MascotaSet]
ADD CONSTRAINT [PK_MascotaSet]
    PRIMARY KEY CLUSTERED ([Id_Mascota] ASC);
GO

-- Creating primary key on [Id_Propietario] in table 'PropietarioSet'
ALTER TABLE [dbo].[PropietarioSet]
ADD CONSTRAINT [PK_PropietarioSet]
    PRIMARY KEY CLUSTERED ([Id_Propietario] ASC);
GO

-- Creating primary key on [Id_Administrador] in table 'AdministradorSet'
ALTER TABLE [dbo].[AdministradorSet]
ADD CONSTRAINT [PK_AdministradorSet]
    PRIMARY KEY CLUSTERED ([Id_Administrador] ASC);
GO

-- Creating primary key on [Id_Pagina] in table 'PaginaSet'
ALTER TABLE [dbo].[PaginaSet]
ADD CONSTRAINT [PK_PaginaSet]
    PRIMARY KEY CLUSTERED ([Id_Pagina] ASC);
GO

-- Creating primary key on [Id_Historia_Clinica] in table 'Historia_ClinicaSet'
ALTER TABLE [dbo].[Historia_ClinicaSet]
ADD CONSTRAINT [PK_Historia_ClinicaSet]
    PRIMARY KEY CLUSTERED ([Id_Historia_Clinica] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------